/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

 import React, {Component} from 'react';
 import {Platform, StyleSheet, Text, View} from 'react-native';
 import { createStackNavigator, createAppContainer } from 'react-navigation';
 import MainScreen from './Components/MainScreen';

  export default class App extends Component {
   render() {
     return (
       <AppContainer />
     );
   }
 }

 const AppStackNavigator = createStackNavigator({

     Home: MainScreen,
  

 });
 const AppContainer = createAppContainer(AppStackNavigator);
 /*import { createStackNavigator, createAppContainer } from 'react-navigation';
 import MainScreen from './Components/MainScreen';

 export default class App extends Component {
   render() {
     return (
       <AppNavigator />
     );
   }
 }

 const AppNavigator = createStackNavigator({

   Main: {
     screen: MainScreen
   }

 });

 const AppContainer = createAppContainer(AppNavigator);

 export default AppContainer;*/
